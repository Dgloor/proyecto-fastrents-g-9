package Interfaz;

import Sistema.Sistema;
import Modelo.Usuario;
import java.util.Scanner;

/**
 *
 * @author Danny Loor
 */
public class SistemaUI {

    private Sistema sistema;
    private Scanner entrada;

    public static void main(String[] args) {
        SistemaUI s = new SistemaUI();
        s.menu();
    }

    public SistemaUI() {
        sistema = new Sistema();
        entrada = new Scanner(System.in);
    }

    public void menu() {
        String op = "";
        while (!"3".equals(op)) {
            System.out.print("\n");
            System.out.println("-- FASTRENTS--");
            System.out.println("1. Iniciar Sesión");
            System.out.println("2. Registrarse");
            System.out.println("3. Salir");
            System.out.print("Ingrese una opción: ");
            op = entrada.nextLine();
            System.out.print("\n");

            switch (op) {
                case "1":
                    iniciarSesion();
                    break;
                case "2":
                    registrarUsuario();
                    break;
                case "3":
                    System.out.println(
                            "</> Gracias por usar nuestro sistema. </>");
                    break;
                default:
                    System.out.println(
                            "<x> Error, seleccione una opción válida. <x>");
            }
        }
    }

    public void iniciarSesion() {
        String usuario = "";
        String contraseña = "";

        System.out.println("Inicio de sesión de usuario");

        while ("".equals(usuario) || "".equals(contraseña)) {
            System.out.print("Username: ");
            usuario = entrada.nextLine();
            System.out.print("Password: ");
            contraseña = entrada.nextLine();

            if ("".equals(usuario) || "".equals(contraseña)) {
                System.out.println(
                        "\n<x> Campos incompletos. <x>\n");
            }
        }  
        
        Usuario u = sistema.iniciarSesion(usuario, contraseña);

        if (u != null) {     
            sistema.incializarSubsistema(u);

        } else {
            System.out.println("\n<x> Usuario NO registrado "
                    + "dentro del sistema. <x>");
        }
    }

    public void registrarUsuario() {
        String id = "";
        String nombre = "";
        String direccion = "";
        String correo = "";
        String username = "";
        String contra = "";

        System.out.println("Información de cuenta");

        while ("".equals(id) || "".equals(nombre) || "".equals(direccion)
                || "".equals(correo) || "".equals(username)
                || "".equals(contra)) {

            System.out.print("No. de identificación: ");
            id = entrada.nextLine();
            System.out.print("Nombre: ");
            nombre = entrada.nextLine();
            System.out.print("Dirección: ");
            direccion = entrada.nextLine();
            System.out.print("Correo electrónico: ");
            correo = entrada.nextLine();
            System.out.print("Username: ");
            username = entrada.nextLine();
            System.out.print("Password: ");
            contra = entrada.nextLine();

            if (("".equals(id) || "".equals(nombre)
                    || "".equals(direccion) || "".equals(correo)
                    || "".equals(username) || "".equals(contra))) {
                System.out.println(""
                        + "\n<x> Error, rellene todos los campos. <x>\n");
            }
        }
        String op = "";
        System.out.println("");
        System.out.println("Registrarse como...");
        System.out.println("1. Rider");
        System.out.println("2. Charger");
        System.out.println("3. Cancelar Registro");

        while ("".equals(op) || !("1".equals(op) || "2".equals(op)
                || "3".equals(op))) {
            System.out.print("Ingrese una opción: ");
            op = entrada.nextLine();

            if ("".equals(op) || !("1".equals(op) || "2".equals(op)
                    || "3".equals(op))) {
                System.out.println(
                        "\n<x> Error, seleccione "
                        + "una opción válida. <x>\n");
            }
        }
        switch (op) {
            case "1":
                registrarRider(id, nombre, direccion,
                        correo, username, contra);
                break;
            case "2":
                registrarCharger(id, nombre, direccion,
                        correo, username, contra);
                break;
            case "3":
                System.out.println(
                        "\n</> Registro no completado. </>");
                break;
            default:
                break;
        }

    }

    public void registrarRider(String id, String n, String d,
            String c, String u, String p) {

        boolean registroExitoso;
        String nombreBanco = "";
        String nTarjeta = "";
        String tTarjeta = "";

        System.out.println("");
        System.out.println("Datos - Tarjeta de crédito");

        while ("".equals(nombreBanco) || "".equals(nTarjeta)
                || "".equals(tTarjeta)) {

            System.out.print("Nombre del banco: ");
            nombreBanco = entrada.nextLine();
            System.out.print("Número de tarjeta: ");
            nTarjeta = entrada.nextLine();
            System.out.print("Titular de la tarjeta: ");
            tTarjeta = entrada.nextLine();
            if ("".equals(nombreBanco) || "".equals(nTarjeta)
                    || "".equals(tTarjeta)) {
                System.out.println(
                        "\n<x> Error, campos incompletos. <x>\n");
            }
        }

        registroExitoso = sistema.registrarUsuario(id, n, d,
                c, u, p, nombreBanco, nTarjeta, tTarjeta);

        if (registroExitoso) {
            System.out.println(
                    "\n</> Rider registrado con éxito. </>");
        } else {
            System.out.println(
                    "\n<x> Error, Rider registrado anteriormente. <x>");
        }
    }

    public void registrarCharger(String id, String n, String d,
            String c, String u, String p) {

        boolean registroExitoso;
        String licencia = "";

        System.out.println("");

        while ("".equals(licencia)) {

            System.out.print("No. de licencia: ");
            licencia = entrada.nextLine();
            if ("".equals(licencia)) {
                System.out.println(
                        "\n<x> Error, campos incompletos. <x>\n");
            }
        }

        registroExitoso = sistema.registrarUsuario(id, n, d,
                c, u, p, licencia);

        if (registroExitoso) {
            System.out.println(
                    "\n</> Charger registrado con éxito. </>");
        } else {
            System.out.println(
                    "\n<x> Error, "
                    + "Charger registrado anteriormente.");
        }
    }
}
