package Interfaz;

import Modelo.EstadoTransporte;
import static Modelo.EstadoTransporte.DISPONIBLE;
import static Modelo.EstadoTransporte.PRESTADO;
import Modelo.Prestamo;
import Modelo.Rider;
import Sistema.SistemaGPS;
import Modelo.Transporte;
import Sistema.SisRider;
import Sistema.Sistema;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Danny Loor
 */
public class RiderUI {

    private Sistema sistema;
    private Rider rider;
    private SisRider sRider;
    private Scanner entrada;

    public RiderUI(Sistema s, Rider r) {
        this.sistema = s;
        this.rider = r;
        entrada = new Scanner(System.in);
        sRider = new SisRider(s);
    }

    public void menu() {
        String op = "";

        while (!"5".equals(op)) {
            System.out.print("\n");
            System.out.println("- Sistema de Renta de transportes -");
            System.out.println("1. Encontrar transportes cercanos");
            System.out.println("2. Rentar transporte");
            System.out.println("3. Devolver transporte");
            System.out.println("4. Ver reporte de préstamos");
            System.out.println("5. Cerrar sesión");
            System.out.print("Ingrese una opción: ");
            op = entrada.nextLine();
            System.out.print("\n");

            switch (op) {
                case "1":
                    encontrarCercanos();
                    break;
                case "2":
                    prestarTransporte();
                    break;
                case "3":
                    devolverTransporte();
                    break;
                case "4":
                    reportarPrestamos();
                    break;
                case "5":
                    System.out.println(
                            "</> Cierre de sesión exitoso. </>");
                    break;
                default:
                    System.out.println(
                            "<x> Error, seleccione una opción válida. <x>");
            }
        }
    }

    public void encontrarCercanos() {
        String direccion = "";
        String rango = "";
        ArrayList<Transporte> tEncontrados;

        while ("".equals(direccion) && "".equals(rango)) {
            System.out.print("Ingrese su ubicación actual: ");
            direccion = entrada.nextLine();
            System.out.print("Rango(km): ");
            rango = entrada.nextLine();

            if ("".equals(direccion) || "".equals(rango)) {
                System.out.println(
                        "\n<x>Error, campos incompletos. <x>\n");
            }
        }

        tEncontrados = sistema.encontrarCercanos(direccion, rango, "R");

        if (tEncontrados.isEmpty()) {
            System.out.println(
                    "\n<x> No se han encontrado transportes"
                    + "\ndentro del rango, <x>");
        } else {

            System.out.println(
                    "\nTransportes disponibles:\n");
            for (Transporte t : tEncontrados) {
                System.out.printf("%-9s   %4s   %-30s   %-2skw   a  %4.2f km\n",
                        t.getClass().getSimpleName(),
                        t.getCodigo(),
                        t.getDireccion(),
                        t.getNivelBateria(),
                        sistema.getDistancia(t,
                                SistemaGPS.consultarUbicacion(direccion)));
            }
        }
    }

    public void prestarTransporte() {
        String codigo = "";
        String confirmar = "";
        EstadoTransporte estado;
        Transporte t;
        boolean puedePrestar = sRider.puedePrestar(rider);

        if (puedePrestar) {

            while ("".equals(codigo)) {
                System.out.print("Ingrese el código del transporte: ");
                codigo = entrada.nextLine();
                if ("".equals(codigo)) {
                    System.out.println(
                            "\n<x> Error, por favor llene el campo. <x>\n");
                }
            }

            estado = sistema.verificarTransporte(codigo);

            if (estado != null) {

                switch (estado) {
                    case CARGANDO:
                        System.out.println(
                                "\n<x> Transporte en carga. <x>");
                        break;

                    case PRESTADO:
                        System.out.println(
                                "\n<x> Transporte en uso. <x>");
                        break;

                    case DISPONIBLE:
                        t = sistema.getTransporte(codigo);

                        // Si su batería es negativa no se lo 
                        // podrá prestar.
                        if (t.getNivelBateria() < 0) {
                            System.out.println("\n<x> Transporte con "
                                    + "batería demasiado baja. <x> ");
                            break;
                        }

                        System.out.println(
                                "\nTransporte: "
                                + t.getClass().getSimpleName());
                        System.out.println(
                                "Nivel de batería: "
                                + t.getNivelBateria() + " kw");

                        do {
                            System.out.print("Confirmar préstamo(S/N): ");
                            confirmar = entrada.nextLine().toUpperCase();
                        } while (!("S".equals(confirmar)
                                || "N".equals(confirmar)));

                        switch (confirmar) {
                            case "S":
                                sRider.prestarTransporte(rider, t);
                                System.out.println(
                                        "\n</> Préstamo iniciado "
                                        + "con éxito </>\n- "
                                        + t.getClass().getSimpleName()
                                        + " lista para usar.");
                                break;
                            case "N":
                                System.out.println(
                                        "\n</> Préstamo cancelado. </>");
                                break;
                            default:
                                break;
                        }
                }
            } else { // En caso de ser un transporte inexistente.
                System.out.println(
                        "\n<x> Error, código inválido. <x>");
            }
        } else {
            System.out.println(
                    "<x> Usted ya se encuentra utilizando"
                    + "\notro transporte. <x>");
        }
    }

    public void devolverTransporte() {
        String codigo = "";
        String direccion = "";
        EstadoTransporte estado;
        Prestamo p;
        Object[] infoPrestamo;
        boolean puedeDevolver = sRider.puedeDevolver(rider);

        if (puedeDevolver) {

            while ("".equals(codigo) || "".equals(codigo)) {

                System.out.print("Ingrese código de transporte: ");
                codigo = entrada.nextLine();
                System.out.print("Ingrese su ubicación actual: ");
                direccion = entrada.nextLine();
                if ("".equals(codigo) || "".equals(direccion)) {
                    System.out.println(
                            "\n<x> Error, campos incompletos. <x>\n");
                }
            }

            estado = sistema.verificarTransporte(codigo);

            if (estado != null) {

                switch (estado) {
                    case CARGANDO:
                        System.out.println(
                                "\n<x> Trasnporte en carga. <x>");
                        break;

                    case DISPONIBLE:
                        System.out.println(
                                "\n<x> Transporte no rentado. <x>");
                        break;

                    case PRESTADO:
                        p = sRider.getPrestamoVerificado(rider, codigo);

                        if (p != null) {
                            Transporte t = sistema.getTransporte(codigo);
                            sRider.terminarPrestamo(p, direccion);
                            infoPrestamo = sRider.obtenerInfoPrestamo(p);
                            mostrarPrestamoFinalizado(infoPrestamo);
                            seleccionarTarjetaPago();

                        } else {
                            System.out.println(
                                    "\n<x> Trasnporte rentado "
                                    + "por otro Rider.");
                        }
                        break;
                }
            } else { // en caso de ser un transporte inexistente.
                System.out.println(
                        "\n<x> Error, código inválido. <x>");
            }
        } else {
            System.out.println("<x> Error, no tiene ningún "
                    + "prestamo activo");
        }
    }

    public void mostrarPrestamoFinalizado(Object[] infoPrestamo) {
        System.out.println("\nTransporte utilizado: "
                + infoPrestamo[0]);
        System.out.printf("Distancia Recorrida: %.2f km",
                infoPrestamo[4]);
        System.out.printf("\nBatería utilizada: %d kw",
                (int) infoPrestamo[6]);
        System.out.println("\nTiempo del préstamo: "
                + infoPrestamo[3] + " min");
        System.out.printf("Total a pagar: $%.2f",
                infoPrestamo[5]);
        System.out.println("");
    }

    public void seleccionarTarjetaPago() {
        String seleccion = "";

        while (!("1".equals(seleccion) || "2".equals(seleccion))) {
            System.out.println(" ");
            System.out.println("Seleccione la tarjeta de Crédito: ");
            System.out.println("    1. " + rider.getNombreBanco()
                    + ", " + rider.getNumeroTarjeta()
                    + ", " + rider.getTitularTarjeta());
            System.out.println("    2. Nueva tarjeta");
            System.out.print("Ingrese una opción: ");
            seleccion = entrada.nextLine();

            if (!("1".equals(seleccion) || "2".equals(seleccion))) {
                System.out.println(
                        "\n<x> Error, seleccione una opción válida. <x>");
            }
        }

        if ("1".equals(seleccion)) {
            System.out.println(
                    "\n</> Pago exitoso, préstamo finalizado. </>");
        } else {
            String nombreBanco = "";
            String nTarjeta = "";
            String titular = "";

            System.out.println("");
            System.out.println("Nueva tarjeta de crédito");

            while ("".equals(nombreBanco) || "".equals(nTarjeta)
                    || "".equals(titular)) {

                System.out.print("Nombre del banco: ");
                nombreBanco = entrada.nextLine();
                System.out.print("Número de tarjeta: ");
                nTarjeta = entrada.nextLine();
                System.out.print("Titular de la tarjeta: ");
                titular = entrada.nextLine();

                if ("".equals(nombreBanco) || "".equals(nTarjeta)
                        || "".equals(titular)) {
                    System.out.println(
                            "\n<x> Error, campos incompletos. <x>\n");
                }
            }
            sRider.cambiarTarjetaCredito(rider, nombreBanco,
                    nTarjeta, titular);
            System.out.println(
                    "\n- Tarjeta registrada con éxito. -"
                    + "\n</> Pago exitoso, préstamo finalizado. </>");
        }
    }

    public void reportarPrestamos() {

        if (rider.getPrestamos().isEmpty()) {
            System.out.println(""
                    + "<x> Usted no ha realizado ningún"
                    + "\npréstamo hasta la fecha. <x>");

        } else {
            String fi = "";
            String ff = "";

            System.out.println("- Préstamos por rango de fecha -");
            System.out.println("Formato requerido: yyyy-mm-dd");

            while ("".equals(fi) || "".equals(ff)) {
                System.out.print("Ingrese fecha de inicio: ");
                fi = entrada.nextLine();
                System.out.print("Ingrese fecha de fin: ");
                ff = entrada.nextLine();

                if ("".equals(fi) || "".equals(ff)) {
                    System.out.println(
                            "\n<x> Error, campos imcompletos. <x>");
                }
            }
            ArrayList<Prestamo> pEncontrados
                    = sRider.buscarPrestamos(rider, fi, ff);

            if (pEncontrados.isEmpty()) {
                System.out.println("n</> No se ha encontrado "
                        + "ningún préstamo \ndentro del rango "
                        + "de fechas introducido. <x>");
            } else {

                System.out.println("\n...Préstamos realizados:");
                for (Prestamo p : pEncontrados) {

                    Object[] infoPrestamo
                            = sRider.obtenerInfoPrestamo(p);

                    System.out.println("\nTransporte utilizado: "
                            + infoPrestamo[0]);
                    System.out.println("Hora y fecha de inicio: "
                            + infoPrestamo[1]);
                    System.out.println("Duración del préstamo: "
                            + infoPrestamo[3] + " min");
                    System.out.printf("Distancia Recorrida: %.2f km",
                            infoPrestamo[4]);
                    System.out.printf("\nValor pagado: $%.2f",
                            infoPrestamo[5]);
                    System.out.println("");
                }
            }
        }

    }
}
