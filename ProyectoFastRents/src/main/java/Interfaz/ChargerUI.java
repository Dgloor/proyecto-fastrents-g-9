package Interfaz;

import Modelo.Carga;
import Modelo.Charger;
import Modelo.EstadoTransporte;
import Sistema.SistemaGPS;
import Modelo.Transporte;
import Sistema.SisCharger;
import Sistema.Sistema;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Danny Loor
 */
public class ChargerUI {

    private Sistema sistema;
    private Charger charger;
    private SisCharger sCharger;
    private Scanner entrada;

    public ChargerUI(Sistema s, Charger c) {
        this.sistema = s;
        this.charger = c;
        entrada = new Scanner(System.in);
        sCharger = new SisCharger(s);
    }

    public void menu() {
        String op = "";

        while (!"5".equals(op)) {
            System.out.print("\n");
            System.out.println("- Sistema de Carga de transportes -");
            System.out.println("1. Encontrar transportes cercanos");
            System.out.println("2. Cargar Transporte");
            System.out.println("3. Terminar carga");
            System.out.println("4. Ver reporte de cargas");
            System.out.println("5. Cerrar sesión");
            System.out.print("Ingrese una opción: ");
            op = entrada.nextLine();
            System.out.print("\n");

            switch (op) {
                case "1":
                    encontrarCercanos();
                    break;
                case "2":
                    cargarTransporte();
                    break;
                case "3":
                    terminarRecarga();
                    break;
                case "4":
                    reportarCargas();
                    break;
                case "5":
                    System.out.println(
                            "</> Cierre de sesión exitoso. </>");
                    break;
                default:
                    System.out.println(
                            "<x> Error, seleccione una opción válida. <x>");
            }
        }
    }

    public void encontrarCercanos() {
        String direccion = "";
        String rango = "";
        ArrayList<Transporte> tEncontrados;

        while ("".equals(direccion) && "".equals(rango)) {
            System.out.print("Ingrese su ubicación actual: ");
            direccion = entrada.nextLine();
            System.out.print("Rango(km): ");
            rango = entrada.nextLine();

            if ("".equals(direccion) || "".equals(rango)) {
                System.out.println(
                        "\n<x> Datos incompletos. <x>\n");
            }
        }

        tEncontrados = sistema.encontrarCercanos(direccion, rango, "C");

        if (tEncontrados.isEmpty()) {
            System.out.println(
                    "\n<x> No se han encontrado transportes"
                    + "\ncon menos del 20% de batería. <x>");
        } else {

            System.out.println(
                    "\nTransportes descargados:\n");
            for (Transporte t : tEncontrados) {
                System.out.printf("%-9s   %4s   %-30s   %-2skw   a  %4.2f km\n",
                        t.getClass().getSimpleName(),
                        t.getCodigo(),
                        t.getDireccion(),
                        t.getNivelBateria(),
                        sistema.getDistancia(t,
                                SistemaGPS.consultarUbicacion(direccion)));
            }
        }
    }

    public void cargarTransporte() {
        String codigo = "";
        String confirmar = "";
        EstadoTransporte estado;
        Transporte t;

        while ("".equals(codigo)) {
            System.out.print("Ingrese el código del transporte: ");
            codigo = entrada.nextLine();
            if ("".equals(codigo)) {
                System.out.println(
                        "\n<x> Error, campos incompletos. <x>\n");
            }
        }

        estado = sistema.verificarTransporte(codigo);

        if (estado != null) {

            switch (estado) {

                case PRESTADO:
                    System.out.println(
                            "\n<x> Transporte en uso. <x>");
                    break;

                case CARGANDO:
                    System.out.println(
                            "\n<x> Transporte en carga. <x>");
                    break;

                case DISPONIBLE:
                    t = sistema.getTransporte(codigo);

                    if (t.getNivelBateria() == 100) {
                        System.out.println("\n <x> Transporte con "
                                + "batería al máximo. <x>");
                        break;
                    }

                    System.out.println(
                            "\nTransporte: "
                            + t.getClass().getSimpleName());
                    System.out.println(
                            "Nivel de batería: "
                            + t.getNivelBateria() + " kw");

                    do {
                        System.out.print("Confirmar carga(S/N): ");
                        confirmar = entrada.nextLine().toUpperCase();
                    } while (!("S".equals(confirmar)
                            || "N".equals(confirmar)));

                    switch (confirmar) {
                        case "S":
                            sCharger.cargarTransporte(charger, t);
                            System.out.println(
                                    "\n</> Carga confirmada <x> "
                                    + "\n- Transporte listo para "
                                    + "llevar a cargar.");
                            break;
                        case "N":
                            System.out.println(
                                    "\n</> Carga cancelada. </>");
                            break;
                        default:
                            break;
                    }
            }
        } else { // En caso de ser un transporte inexistente.
            System.out.println(
                    "\n<x> Error, código inválido. <x>");
        }
    }

    public void terminarRecarga() {
        String codigo = "";
        String direccion = "";
        String bateriaFinal = "";
        EstadoTransporte estado;
        Carga cg;
        Object[] infoCarga;
        boolean puedeDevolver = sCharger.puedeDevolver(charger);

        if (puedeDevolver) {

            while ("".equals(codigo) || "".equals(codigo)
                    || "".equals(bateriaFinal)) {

                System.out.print("Ingrese código de transporte: ");
                codigo = entrada.nextLine();
                System.out.print("Ubicación final: ");
                direccion = entrada.nextLine();
                System.out.print("Nivel de batería actual(kw): ");
                bateriaFinal = entrada.nextLine();

                if ("".equals(codigo) || "".equals(direccion)
                        || "".equals(bateriaFinal)) {
                    System.out.println(
                            "\n<x> Error, campos incompletos. <x>\n");
                }
            }
            estado = sistema.verificarTransporte(codigo);

            if (estado != null) {

                switch (estado) {
                    case DISPONIBLE:
                        System.out.println(
                                "\n<x> Transporte sin carga activa. <x>");
                        break;

                    case PRESTADO:
                        System.out.println(
                                "\n<x> Transporte rentado. <x>");
                        break;

                    case CARGANDO:
                        cg = sCharger.getCargaVerificada(charger, codigo);

                        if (cg != null) {
                            sCharger.terminarReCarga(
                                    cg, direccion, bateriaFinal);
                            System.out.println(
                                    "\n</> Orden de pago generada. </>");
                            System.out.printf(
                                    "- Pago por la carga: $%.2f",
                                    cg.getValorPago());
                            System.out.println("");

                        } else {
                            System.out.println(
                                    "\n<x> Transporte siendo cargado"
                                    + "por otro Charger. <x>");
                        }
                }
            } else {
                System.out.println(
                        "\n<x> Error, código inválido. <x>");
            }
        } else {
            System.out.println(
                    "<x> Error, no tiene ninguna carga activa.");
        }
    }

    public void reportarCargas() {
        if (charger.getCargas().isEmpty()) {
            System.out.println(""
                    + "<x> Usted no ha realizado ninguna"
                    + "\ncarga hasta la fecha. <x>");
        } else {
            String fi = "";
            String ff = "";

            System.out.println("- Préstamos por rango de fecha -");
            System.out.println("Formato requerido: yyyy-mm-dd");

            while ("".equals(fi) || "".equals(ff)) {
                System.out.print("Ingrese fecha de inicio: ");
                fi = entrada.nextLine();
                System.out.print("Ingrese fecha de fin: ");
                ff = entrada.nextLine();

                if ("".equals(fi) || "".equals(ff)) {
                    System.out.println(
                            "\n<x> Error, campos imcompletos. <x>");
                }
            }
            ArrayList<Carga> cgEncontradas
                    = sCharger.buscarCargas(charger, fi, ff);

            if (cgEncontradas.isEmpty()) {
                System.out.println("n</> No se ha encontrado "
                        + "ninguna carga \ndentro del rango "
                        + "de fechas introducido. <x>");
            } else {

                double totalPagoRango = 0; // Suma de los pagos    
                System.out.println("\n...Cargas realizadas:");

                for (Carga cg : cgEncontradas) {

                    Object[] infoCarga = sCharger.obtenerInfoCarga(cg);

                    System.out.println("\nHora y fecha de inicio: "
                            + infoCarga[0]);
                    System.out.println("Transporte utilizado: "
                            + infoCarga[1] + " / " + infoCarga[2]);
                    System.out.printf("Valor recibido: $%.2f",
                            infoCarga[3]);
                    System.out.println("");
                    totalPagoRango += (double) infoCarga[3];
                }

                System.out.printf("\n- El pago total durante el rango"
                        + "\nde tiempo fue de : $%.2f", totalPagoRango);
                System.out.println("");
            }
        }

    }
}
