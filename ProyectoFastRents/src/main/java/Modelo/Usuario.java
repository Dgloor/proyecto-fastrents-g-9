package Modelo;

/**
 *
 * @author Danny Loor
 */
public class Usuario {

    // Atributos
    private String id;
    private String nombre;
    private String direccion;
    private String correo;
    private String username;
    private String password;

    // Constructor
    public Usuario(String id, String nombre, String direccion, String correo,
            String username, String password) {
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
        this.username = username;
        this.password = password;
    }

    // Métodos
    // Getter & Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Método que compara un objeto que es pasado como parametro. Si es una
     * intancia de Usuario, será igual a otro usuario en caso de que tengan el
     * mismo número de identificación.
     *
     * @param obj, objeto a comparar
     * @return true si son iguales, false caso contrario.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj.getClass() == getClass()) {
            Usuario u = (Usuario) obj;
            return u.getId().equals(getId());
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "\nId: " + id + "\nNombre: " + nombre
                + "\nDirección: " + direccion + "\nCorreo: " + correo
                + "\nUsername: " + username + "\nPassword: " + password;
    }
}
