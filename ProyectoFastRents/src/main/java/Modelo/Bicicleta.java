package Modelo;

/**
 *
 * @author Danny Loor
 */
public class Bicicleta extends Transporte {

    // Constructor
    public Bicicleta(String codigo, String direccion) {
        super(codigo, 7, direccion, 0.20);
    }

}
