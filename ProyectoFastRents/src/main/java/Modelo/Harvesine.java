package Modelo;

/**
 *
 * @author Danny Loor
 */
public class Harvesine {

    public static final double R = 6372.8; // Radio de la tierra en Km

    /**
     * Método estático que procede a calcular la distancia entre dos Ubicaciones
     * pasadas como parámetro
     *
     * @param u1 ubicacion
     * @param u2 ubicacion
     * @return El número de km que existe entre ambos puntos.
     */
    public static double calcularDistancia(Ubicacion u1, Ubicacion u2) {
        double lat1 = u1.getLatitud();
        double lon1 = u1.getLonguitud();
        double lat2 = u2.getLatitud();
        double lon2 = u2.getLonguitud();

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2), 2)
                + Math.pow(Math.sin(dLon / 2), 2)
                * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));

        return R * c;
    }
}
