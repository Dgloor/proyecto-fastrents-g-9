package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Danny Loor
 */
public class Charger extends Usuario {

    private String licencia;
    private ArrayList<Carga> cargas;

    // Constructor
    public Charger(String id, String nombre, String direccion, String correo,
            String username, String password, String licencia) {

        super(id, nombre, direccion, correo, username, password);
        this.licencia = licencia;
        cargas = new ArrayList<Carga>();
    }

    // Getters & Setters
    public String getLicencia() {
        return licencia;
    }

    public ArrayList<Carga> getCargas() {
        return cargas;
    }

     public void addCarga(Carga carga) {
        cargas.add(carga);
    }

}
