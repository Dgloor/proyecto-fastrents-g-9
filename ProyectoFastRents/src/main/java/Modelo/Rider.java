package Modelo;

import java.util.ArrayList;

/**
 *
 * @author Danny Loor
 */
public class Rider extends Usuario {

    private String nombreBanco;
    private String numeroTarjeta;
    private String titularTarjeta;
    private ArrayList<Prestamo> prestamos;

    // Constructor
    public Rider(String id, String nombre, String direccion, String correo,
            String username, String password, String nombreBanco,
            String numeroTarjeta, String titularTarjeta) {

        super(id, nombre, direccion, correo, username, password);
        this.nombreBanco = nombreBanco;
        this.numeroTarjeta = numeroTarjeta;
        this.titularTarjeta = titularTarjeta;
        prestamos = new ArrayList<Prestamo>();
    }

    // Getter & Setters
    public String getNombreBanco() {
        return nombreBanco;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public String getTitularTarjeta() {
        return titularTarjeta;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public void setTitularTarjeta(String titularTarjeta) {
        this.titularTarjeta = titularTarjeta;
    }

    public ArrayList<Prestamo> getPrestamos() {
        return prestamos;
    }

    public void addPrestamo(Prestamo prestamo) {
        prestamos.add(prestamo);
    }

}
