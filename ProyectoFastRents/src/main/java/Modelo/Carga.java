package Modelo;

import java.time.LocalDateTime;

/**
 *
 * @author Danny Loor
 */
public class Carga {

    private Charger charger;
    private Transporte transporte;
    private LocalDateTime horaFecha_Inicio;
    private LocalDateTime horaFecha_Fin;
    private double valorPago;
    private Ubicacion ubicacionInicio;
    private Ubicacion ubicacionFin;
    private boolean activa;

    // Constructor
    public Carga(Charger charger, Transporte transporte,
            LocalDateTime horaFecha_Inicio) {
        this.charger = charger;
        this.transporte = transporte;
        this.horaFecha_Inicio = horaFecha_Inicio;
        this.ubicacionInicio = transporte.getUbicacion();
        activa = true;
    }

    /**
     * Termina una carga, setea la fecha fin a la fecha actual, coloca el
     * transporte en la ubicación pasada como parametro, y ademas llama a
     * calcularValor pasandole la batería final del transporte para así poder
     * determinar cuanto se pagará por la carga.
     *
     * @param ubiEntrega ubicacion final
     * @param ff fecha final
     * @param bateriaFinal bF
     */
    public void terminarCarga(Ubicacion ubiEntrega,
            LocalDateTime ff, int bateriaFinal) {
        horaFecha_Fin = ff;
        ubicacionFin = ubiEntrega;
        activa = false;
        valorPago = calcularValor(bateriaFinal);
        transporte.devolver(ubiEntrega, bateriaFinal);
    }

    /**
     ** Calcula el valor que debe pagarse por la carga dependiendo de los kw
     * suministrados al transporte.
     *
     * @param bateriaFinal del transporte cargado.
     * @return valor a Pagar
     */
    public double calcularValor(int bateriaFinal) {
        int kwSuministrados = bateriaFinal - transporte.getNivelBateria();
        double valor = kwSuministrados * 0.15;
        return valor;
    }

    // Getter & Setters
    public Charger getCharger() {
        return charger;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    public LocalDateTime getHoraFecha_Inicio() {
        return horaFecha_Inicio;
    }

    public LocalDateTime getHoraFecha_Fin() {
        return horaFecha_Fin;
    }

    public double getValorPago() {
        return valorPago;
    }

    public Ubicacion getUbicacionInicio() {
        return ubicacionInicio;
    }

    public Ubicacion getUbicacionFin() {
        return ubicacionFin;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setCharger(Charger charger) {
        this.charger = charger;
    }

    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }

    public void setHoraFecha_Inicio(LocalDateTime horaFecha_Inicio) {
        this.horaFecha_Inicio = horaFecha_Inicio;
    }

    public void setHoraFecha_Fin(LocalDateTime horaFecha_Fin) {
        this.horaFecha_Fin = horaFecha_Fin;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public void setUbicacionInicio(Ubicacion ubicacionInicio) {
        this.ubicacionInicio = ubicacionInicio;
    }

    public void setUbicacionFin(Ubicacion ubicacionFin) {
        this.ubicacionFin = ubicacionFin;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    // toString
    @Override
    public String toString() {
        return "-- Carga --"
                + "\nEstado" + activa
                + "\nCharger: " + charger
                + "\nTransporte: " + transporte
                + "\nHora de inicio: " + horaFecha_Inicio
                + "\nHora de finalización: " + horaFecha_Fin
                + "\nUbicación inicial: " + ubicacionInicio
                + "\nUbicación final: " + ubicacionFin
                + "\nValor a pagar: " + valorPago;
    }

}
