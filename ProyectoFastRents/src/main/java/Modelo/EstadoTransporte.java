package Modelo;

/**
 * Contiene los únicos 3 estados que puede tomar un transporte.
 *
 * @author Danny Loor
 */
public enum EstadoTransporte {
    DISPONIBLE, PRESTADO, CARGANDO;
}
