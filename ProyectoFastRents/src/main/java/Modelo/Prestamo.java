package Modelo;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author Danny Loor
 */
public class Prestamo {

    private Rider rider;
    private Transporte transporte;
    private LocalDateTime horaFecha_Inicio;
    private LocalDateTime horaFecha_Fin;
    private double valorPrestamo;
    private Ubicacion ubicacionInicio;
    private Ubicacion ubicacionFin;
    private boolean activo;

    // Constructor
    public Prestamo(Rider rider, Transporte transporte,
            LocalDateTime horaFecha_Inicio) {
        this.rider = rider;
        this.transporte = transporte;
        this.horaFecha_Inicio = horaFecha_Inicio;
        ubicacionInicio = transporte.getUbicacion();
        activo = true;
    }

    /**
     * Termina un préstamo, llama a otra función para que calcule el valor
     * total, ubica al transporte en la ubicación pasada como parmetro, por
     * últmo cambia el estado del prestamo a inactivo
     *
     * @param ubiFinal uF
     * @param ff fecha/hora de culminacion de prestamo.
     */
    public void terminarPrestamo(Ubicacion ubiFinal, LocalDateTime ff) {
        horaFecha_Fin = ff;
        ubicacionFin = ubiFinal;
        valorPrestamo = calcularValor();
        activo = false;
        transporte.devolver(ubicacionFin,
                Harvesine.calcularDistancia(ubicacionInicio, ubicacionFin));
    }

    /**
     * Calcula el valor que debe cobrarse por el préstamo dependiendo de los
     * minutos que haya usado el transporte.
     *
     * @return valor a pagar.
     */
    public double calcularValor() {
        long minutos = ChronoUnit.MINUTES.between(
                horaFecha_Inicio, horaFecha_Fin);
        double valor = transporte.getCostoMinuto() * minutos + 1.00;
        return valor;
    }

    // Getter & Setters
    public Rider getRider() {
        return rider;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    public LocalDateTime getHoraFecha_Inicio() {
        return horaFecha_Inicio;
    }

    public LocalDateTime getHoraFecha_Fin() {
        return horaFecha_Fin;
    }

    public double getValorPago() {
        return valorPrestamo;
    }

    public Ubicacion getUbicacionInicio() {
        return ubicacionInicio;
    }

    public Ubicacion getUbicacionFin() {
        return ubicacionFin;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setRider(Rider rider) {
        this.rider = rider;
    }

    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }

    public void setHoraFecha_Inicio(LocalDateTime horaFecha_Inicio) {
        this.horaFecha_Inicio = horaFecha_Inicio;
    }

    public void setHoraFecha_Fin(LocalDateTime horaFecha_Fin) {
        this.horaFecha_Fin = horaFecha_Fin;
    }

    public void setValorPago(double valorPago) {
        this.valorPrestamo = valorPago;
    }

    public void setUbicacionInicio(Ubicacion ubicacionInicio) {
        this.ubicacionInicio = ubicacionInicio;
    }

    public void setUbicacionFin(Ubicacion ubicacionFin) {
        this.ubicacionFin = ubicacionFin;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    // toString
    @Override
    public String toString() {
        return "-- Prestamo --"
                + "\nEstado: " + activo
                + "\nRider: " + rider
                + "\nTransporte: " + transporte
                + "\nHora de inicio: " + horaFecha_Inicio
                + "\nHora de finalización: " + horaFecha_Fin
                + "\nUbicación inicial: " + ubicacionInicio
                + "\nUbicación final: " + ubicacionFin
                + "\nValor a pagar: " + valorPrestamo;
    }

}
