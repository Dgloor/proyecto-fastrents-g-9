package Modelo;

import Sistema.SistemaGPS;
import static Modelo.EstadoTransporte.CARGANDO;
import static Modelo.EstadoTransporte.DISPONIBLE;
import static Modelo.EstadoTransporte.PRESTADO;

/**
 *
 * @author Danny Loor
 */
public class Transporte {

    // Atributos
    private String codigo;
    private EstadoTransporte estado;
    private int nivelBateria;
    private final int bateriaMax;
    private int consumo;
    private Ubicacion ubicacion;
    private double costoMinuto;

    // Constructor
    public Transporte(String codigo, int consumo, String direccion,
            double costoMinuto) {
        this.codigo = codigo;
        estado = DISPONIBLE;
        nivelBateria = 100;
        bateriaMax = 100;
        this.consumo = consumo;
        this.ubicacion = SistemaGPS.consultarUbicacion(direccion);
        this.costoMinuto = costoMinuto;
    }

    /**
     * Se comienza a prestar el transporte y este entra en un estado de
     * préstamo.
     *
     * @return true si se cambió su estado, false caso contrario.
     */
    public boolean rentar() {
        if (estado == DISPONIBLE) {
            estado = PRESTADO;
            return true;
        }
        return false;
    }

    /**
     * Se empieza a cargar un transporte y este entra en un estado de carga.
     *
     * @return true si se cambió su estado, false caso contrario.
     */
    public boolean cargar() {
        if (estado == DISPONIBLE) {
            estado = CARGANDO;
            return true;
        }
        return false;
    }

    /**
     * (Rider) Método que procede a devolver un transporte, seteando su
     * ubicación a la pasada como parámetro, y su nivel de batería lo setea
     * dependiendo de cuantos km haya recorrido.
     *
     * @param u, Ubicacion final.
     * @param km, distancia recorrida durante el préstamo.
     * @return true si se pudo devolver, false caso contrario.
     */
    public boolean devolver(Ubicacion u, double km) {
        if (estado == PRESTADO) {
            ubicacion = u;
            estado = DISPONIBLE;
            nivelBateria = nivelBateria - calcularConsumo(km);
            if (nivelBateria <= 0) {
                nivelBateria = 0;
            }
            return true;
        }
        return false;
    }

    /**
     * (Charger) Método que procede a devolver un transporte, cambiando su
     * estado a disponible, el nivel de batería, al nivel pasado como parametro
     * y lo setea en la ubicación final.
     *
     * @param u, Ubicación donde es dejado el transporte.
     * @param bateria, nivel de batería final.
     * @return true si se devolvió correctamente, false caso contrario.
     */
    public boolean devolver(Ubicacion u, int bateria) {
        if (estado == CARGANDO) {
            estado = DISPONIBLE;
            nivelBateria = bateria;
            ubicacion = u;
            return true;
        }
        return false;
    }

    /**
     * Método que permite conocer el consumo total de un transporte que fue
     * prestado.
     *
     * @param kmRecorridos double
     * @return La batería que fue usada en el recorrido.
     */
    public int calcularConsumo(double kmRecorridos) {
        int bateriaConsumida = (int) kmRecorridos * consumo;
        return bateriaConsumida;
    }

    public Object[] reportarInfo() {
        Object listaInfo[] = new Object[3];
        listaInfo[0] = ubicacion;
        listaInfo[1] = nivelBateria;
        listaInfo[2] = estado;
        return listaInfo;
    }

    // Getter & Setters
    public String getCodigo() {
        return codigo;
    }

    public EstadoTransporte getEstado() {
        return estado;
    }

    public int getNivelBateria() {
        return nivelBateria;
    }

    public int getBateriaMax() {
        return bateriaMax;
    }

    public int getConsumo() {
        return consumo;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    /**
     * Método que convierte la Ubicación de un transporte a un String dirección
     *
     * @return direccion del transporte
     */
    public String getDireccion() {
        String direccion;
        direccion = SistemaGPS.consultarUbicacion(ubicacion);
        return direccion.split(",")[0];
    }

    public double getCostoMinuto() {
        return costoMinuto;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setEstado(EstadoTransporte estado) {
        this.estado = estado;
    }

    public void setNivelBateria(int nivelBateria) {
        this.nivelBateria = nivelBateria;
    }

    public void setConsumo(int consumo) {
        this.consumo = consumo;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setCostoMinuto(double costoMinuto) {
        this.costoMinuto = costoMinuto;
    }

    /**
     * Dos transportes serán iguales sólo si tienen el mismo código.
     *
     * @param obj Objeto 
     * @return true si son iguales, false caso contrario
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else if (obj.getClass() == getClass()) {
            Transporte t = (Transporte) obj;
            return t.getCodigo().equals(getCodigo());
        } else {
            return false;
        }
    }

    // Método toString
    @Override
    public String toString() {
        return "\nCódigo: " + codigo + "\nEstado: " + estado
                + "\nNivel de batería: " + nivelBateria + "%"
                + "\nConsumo: " + consumo + " kw/km"
                + "\nUbicación: " + getDireccion();
    }
}
