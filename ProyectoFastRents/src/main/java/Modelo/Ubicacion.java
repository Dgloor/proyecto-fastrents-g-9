package Modelo;

/**
 *
 * @author Danny Loor
 */
public class Ubicacion {

    // Atributos
    private double latitud;
    private double longuitud;

    // Constructor
    public Ubicacion(double latitud, double longuitud) {
        this.latitud = latitud;
        this.longuitud = longuitud;
    }

    // Métodos
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLonguitud() {
        return longuitud;
    }

    public void setLonguitud(double longuitud) {
        this.longuitud = longuitud;
    }

    @Override
    public String toString() {
        return "\nLatitud: " + latitud + "\nLonguitud: " + longuitud;
    }

}
