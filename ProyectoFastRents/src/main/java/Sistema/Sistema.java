package Sistema;

import Interfaz.ChargerUI;
import Interfaz.RiderUI;
import Modelo.Bicicleta;
import Modelo.Charger;
import Modelo.EstadoTransporte;
import static Modelo.EstadoTransporte.DISPONIBLE;
import Modelo.Harvesine;
import Modelo.Rider;
import Modelo.Scooter;
import Modelo.Transporte;
import Modelo.Ubicacion;
import Modelo.Usuario;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author Danny Loor
 */
public class Sistema {

    ArrayList<Transporte> transportes;
    ArrayList<Usuario> usuarios;

    public Sistema() {
        transportes = new ArrayList<>();
        usuarios = new ArrayList<>();
        inicializarSistema();
    }

    private void inicializarSistema() {
        transportes.add(new Bicicleta("b001", "Lago Espol"));
        transportes.add(new Bicicleta("b002", "City Mall"));
        transportes.add(new Bicicleta("b003", "FIEC, ESPOL"));
        transportes.add(new Bicicleta("b004", "Biblioteca, ESPOL"));
        transportes.add(new Scooter("s001", "ESPOL admisiones"));
        transportes.add(new Scooter("s002", "Riocentro Ceibos"));
        transportes.add(new Scooter("s003", "Liceo Cristiano de Guayaquil"));
        transportes.add(new Scooter("s004", "Mall del sol"));

        usuarios.add(new Charger("0945578912", "Elon Musk", "Ceibos",
                "muskelon@espol.edu.ec", "charger1", "charger1",
                "4578945211"));
        usuarios.add(new Charger("0953496430", "Edu Smith", "Daule",
                "dgloor@espol.edu.ec", "charger2", "charger2",
                "2123132132"));
        usuarios.add(new Rider("0953496437", "Danny Loor", "La puntilla",
                "dgloor@espol.edu.ec", "rider1", "rider1",
                "Banco Pacifico", "150235121215", "Danny Loor"));
        usuarios.add(new Rider("0945345741", "Max Steel", "Urdenor",
                "maxfsteel@espol.edu.ec", "rider2", "rider2",
                "Banco Pichincha", "145456424315", "Barbie Smith"));
    }

    public Usuario iniciarSesion(String usuario, String contraseña) {
        for (Usuario u : usuarios) {
            if (u.getUsername().equals(usuario)
                    && u.getPassword().equals(contraseña)) {
                return u;
            }
        }
        return null;
    }

    // Los registros serán exitosos solamente si los usuarios creados
    // son nuevos dentro del sistema.
    public boolean registrarUsuario(String id, String nombre,
            String direccion, String correo, String username,
            String password, String nombreBanco, String nTarjeta,
            String titularTarjeta) {

        Rider r = new Rider(id, nombre, direccion, correo,
                username, password, nombreBanco, nTarjeta,
                titularTarjeta);

        if (!usuarios.contains(r)) {
            usuarios.add(r);
            return true;
        }
        return false;
    }

    public boolean registrarUsuario(String id, String nombre,
            String direccion, String correo, String username,
            String password, String licencia) {

        Charger c = new Charger(id, nombre, direccion, correo,
                username, password, licencia);

        if (!usuarios.contains(c)) {
            usuarios.add(c);
            return true;
        }
        return false;
    }

    /**
     * Función que recibe un usuario y dependiendo de que tipo es, instancia a
     * la debida "interfaz".
     *
     * @param u Usuario
     */
    public void incializarSubsistema(Usuario u) {
        if (u instanceof Rider) {
            System.out.println(
                    "\n</> Inicio de sesión exitoso / Rider </>");
            RiderUI rUI = new RiderUI(this, (Rider) u);
            rUI.menu();
        } else if (u instanceof Charger) {
            System.out.println(
                    "\n</> Inicio de sesión exitoso / Charger </>");

            ChargerUI cUI = new ChargerUI(this, (Charger) u);
            cUI.menu();
        }
    }

    /**
     * Función que recibe el código de un transporte existente y retorna el
     * transporte asociado a ese código.
     *
     * @param codigo del tranporte
     * @return Transporte que se busca.
     */
    public Transporte getTransporte(String codigo) {
        for (Transporte t : transportes) {
            if (t.getCodigo().equals(codigo)) {
                return t;
            }
        }
        return null;
    }

    /**
     * Función que recibe una direccion y un rango y busca los transportes
     * disponibles que estén ubicados dentro de ese rango, si los hay... los
     * agrega a un ArrayList. Los transportes retornados varían dependiendo de
     * quién quiera encontrar los transportes cercanos
     *
     * @param direccion String
     * @param rango de fechas
     * @param paraQuien se va a mostrar la Info
     * @return Transportes dentro del rango
     */
    public ArrayList<Transporte> encontrarCercanos(String direccion,
            String rango, String paraQuien) {

        ArrayList<Transporte> tEncontrados = new ArrayList();
        Ubicacion u = SistemaGPS.consultarUbicacion(direccion);

        for (Transporte t : transportes) {

            if ("R".equals(paraQuien)) {
                if (t.getEstado() == DISPONIBLE
                        && getDistancia(t, u)
                        <= Double.parseDouble(rango)) {
                    tEncontrados.add(t);
                }

            } else if ("C".equals(paraQuien)) {
                if (t.getEstado() == DISPONIBLE
                        && (t.getNivelBateria() < 20)
                        && getDistancia(t, u)
                        <= Double.parseDouble(rango)) {
                    tEncontrados.add(t);
                }
            }
        }
        ordenarTransportes(tEncontrados, u);
        return tEncontrados;
    }

    /**
     * Método que recibe un transporte y una Ubicación y calcula la distancia
     * que existe de ese punto al transporte.
     *
     * @param t Transporte
     * @param u ubicación
     * @return double distancia
     */
    public double getDistancia(Transporte t, Ubicacion u) {
        // Llevo la dirección a Ubicacion(lat, long)
        double distancia
                = Harvesine.calcularDistancia(u, t.getUbicacion());
        return distancia;
    }

    /**
     * Método que ordena un ArrayList de Transportes, posicionandolos del más
     * cercano al más lejano con respecto a unaUbicación x. - Ordenamiento
     * burbuja ;)
     *
     * @param t Transporte
     * @param u Ubicación
     */
    public void ordenarTransportes(ArrayList<Transporte> t,
            Ubicacion u) {

        for (int i = 0; i < (t.size() - 1); i++) {
            for (int j = 0; j < (t.size() - 1); j++) {
                if (getDistancia(t.get(j), u)
                        > getDistancia(t.get(j + 1), u)) {
                    Transporte aux = t.get(j);
                    t.set(j, t.get(j + 1));
                    t.set(j + 1, aux);
                }
            }
        }
    }

    /**
     * Función que recibe el código de un transporte, valida que este exista y
     * retorna su estado, caso contrario retorna null.
     *
     * @param codigo del transporte
     * @return Estado de dicho transporte
     */
    public EstadoTransporte verificarTransporte(String codigo) {
        for (Transporte t : transportes) {
            if (t.getCodigo().equals(codigo)) {
                return t.getEstado();
            }
        }
        return null;
    }

    /**
     * Convierte un String fecha a un ibjeto LocalDateTime
     *
     * @param f fecha como String
     * @return fecha como objeto
     */
    public LocalDateTime convertirAFecha(String f) {
        DateTimeFormatter formato;
        LocalDateTime fecha;

        formato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        fecha = LocalDateTime.parse(f.concat(" 00:00"), formato);
        return fecha;
    }

    public ArrayList<Transporte> getTransportes() {
        return transportes;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

}
