package Sistema;

import Modelo.Carga;
import Modelo.Charger;
import Modelo.Transporte;
import Modelo.Ubicacion;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *
 * @author Danny Loor
 */
public class SisCharger {

    Sistema sistema;

    public SisCharger(Sistema s) {
        this.sistema = s;
    }

    /**
     * Se empieza a cargar un transporte y además se añade este registro al
     * arreglo de cargas del Charger
     *
     * @param c Charger que va a cagar el transporte
     * @param t Transporte por cargar
     */
    public void cargarTransporte(Charger c, Transporte t) {
        LocalDateTime hfI = LocalDateTime.now();
        Carga cg = new Carga(c, t, hfI);
        t.cargar();
        c.addCarga(cg);
    }

    /**
     * Un Charger puede devolver un transporte sólo si tiene // como mínimo 1
     * carga activa.
     *
     * @param c Charger
     * @return true si puede, false caso contrario.
     */
    public boolean puedeDevolver(Charger c) {
        if (!c.getCargas().isEmpty()) {
            for (Carga cg : c.getCargas()) {
                if (cg.isActiva()) {
                    return true;
                }
            }
        }
        return false; // En caso de no ha haber cargado nunca.
    }

    /**
     * Función que recibe un Charger que YA HA REALIZADO CARGAS y un código
     * asociado a un transporte. Verifica que él tenga una carga activa con
     * dicho código y retorna esa carga, caso contrario retorna null.
     *
     * @param c Charger
     * @param codigo codigo del transporte
     * @return Carga verificada
     */
    public Carga getCargaVerificada(Charger c, String codigo) {
        for (Carga cg : c.getCargas()) {
            if (cg.getTransporte().getCodigo().equals(codigo)
                    && cg.isActiva()) {
                return cg;
            }
        }
        return null;
    }

    /**
     * Función que recibe una Carga verificada y un String dirección, // y un
     * nivel de batería final, lleva esa direccion a coordenadas // y procede a
     * terminar la carga.
     *
     * @param cg carga
     * @param Direccion String
     * @param bateriaFinal int
     */
    public void terminarReCarga(Carga cg, String Direccion,
            String bateriaFinal) {
        Ubicacion u = SistemaGPS.consultarUbicacion(Direccion);
        LocalDateTime hfF = LocalDateTime.now();
        cg.terminarCarga(u, hfF, Integer.valueOf(bateriaFinal));
    }

    /**
     * Función que recibe un Charger que YA HA REALIZADO CARGAS y dos Strings
     * que corresponden a un rango de fechas. Compara la fecha de cada uno de
     * las asociadas a ese Charger y los agrega a un ArrayList en caso de estar
     * dentro del rango establecido. Al final retorna el arreglo, o null si no
     * existen préstamos en ese rango.
     *
     * @param c Charger
     * @param fi fecha, hora de inicio
     * @param ff fecha, hora de fin
     * @return Arreglo de cargas
     */
    public ArrayList<Carga> buscarCargas(Charger c, String fi,
            String ff) {
        ArrayList<Carga> cgEncontradas = new ArrayList();

        // Conversión de String a LocalDateTime
        LocalDateTime inicio = sistema.convertirAFecha(fi);
        LocalDateTime fin = sistema.convertirAFecha(ff);

        for (Carga cg : c.getCargas()) {
            if (cg.getHoraFecha_Inicio().isAfter(inicio)
                    && cg.getHoraFecha_Fin().isBefore(fin)) {
                cgEncontradas.add(cg);
            }
        }
        ordenarCargas(cgEncontradas);
        return cgEncontradas;
    }

    /**
     * Método que orderna (invierte) un ArrayList de Cargas, posicionándolas de
     * la más reciente a la más antigua.
     *
     * @param c Arreglo de cargas
     */
    public void ordenarCargas(ArrayList<Carga> c) {
        // Guarda la última carga en una variable 
        Carga aux = c.get(c.size() - 1);
        // Inserta al principio la última carga
        c.add(0, aux);
        // Elimina la última carga
        c.remove(c.size() - 1);
    }

    /**
     * Función que recibe una carga finalizada y retorna una lista de objetos
     * con la información de esa carga.
     *
     * @param cg carga
     * @return información de la carga: Fecha, tipo de transporte, código del
     * mismoy el valor pagado
     */
    public Object[] obtenerInfoCarga(Carga cg) {
        Object infoCarga[] = new Object[4];

        // Fecha - Hora de inicio 
        infoCarga[0] = cg.getHoraFecha_Inicio();

        // Tipo de transporte
        infoCarga[1] = cg.getTransporte().getClass().getSimpleName();

        // Código del transporte
        infoCarga[2] = cg.getTransporte().getCodigo();

        // Valor de pago --> double
        infoCarga[3] = cg.getValorPago();

        return infoCarga;
    }
}
