package Sistema;

import Modelo.Harvesine;
import Modelo.Prestamo;
import Modelo.Rider;
import Modelo.Transporte;
import Modelo.Ubicacion;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 *
 * @author Danny Loor
 */
public class SisRider {

    Sistema sistema;

    public SisRider(Sistema s) {
        this.sistema = s;
    }

    /**
     * Un Rider puede prestar un transporte sólo si no tiene un préstamo //
     * activo o en el peor de los casos si no ha prestado nunca.
     *
     * @param r Rider
     * @return true si se pudo prestar, false caso contrario
     */
    public boolean puedePrestar(Rider r) {
        if (r.getPrestamos().isEmpty()) {
            return true;
        } else {
            for (Prestamo p : r.getPrestamos()) {
                if (p.isActivo()) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Un rider puede devolver un transporte sólo si tiene 1 préstamo activo.
     *
     * @param r Rider
     * @return true si se pudo devolver, false caso contrario
     */
    public boolean puedeDevolver(Rider r) {
        if (!r.getPrestamos().isEmpty()) {
            for (Prestamo p : r.getPrestamos()) {
                if (p.isActivo()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void prestarTransporte(Rider r, Transporte t) {
        LocalDateTime hfI = LocalDateTime.now();
        Prestamo p = new Prestamo(r, t, hfI);
        t.rentar();
        r.addPrestamo(p);
    }

    /**
     * Función que recibe un Rider que YA HA REALIZADO PRÉSTAMOS y un código
     * asociado a un transporte. Verifica que él tenga un préstamo activo con
     * dicho código y retorna ese prestamo, caso contrario retorna null.
     *
     * @param r Rider
     * @param codigo de Transporte
     * @return Prestamo Verificado
     */
    public Prestamo getPrestamoVerificado(Rider r, String codigo) {
        for (Prestamo p : r.getPrestamos()) {
            if (p.getTransporte().getCodigo().equals(codigo)
                    && p.isActivo()) {
                return p;
            }
        }
        return null;
    }

    /**
     * Función que recibe un Prestamo verificado y un String dirección. Lleva
     * esa direccion a coordenadas y procede a terminar el préstamo.
     *
     * @param p Prestamo
     * @param Direccion String
     */
    public void terminarPrestamo(Prestamo p, String Direccion) {
        Ubicacion u = SistemaGPS.consultarUbicacion(Direccion);
        LocalDateTime hfF = LocalDateTime.now();
        p.terminarPrestamo(u, hfF);

    }

    /**
     * Función que recibe un préstamo finalizado y retorna una lista de objetos
     * con la información de ese préstamo.
     *
     * @param p Prestamo
     * @return Objetos / info del préstamo
     */
    public Object[] obtenerInfoPrestamo(Prestamo p) {
        Object infoPrestamo[] = new Object[7];

        // Tipo de transporte
        infoPrestamo[0] = p.getTransporte().getClass().getSimpleName();

        // Fecha - Hora de inicio y fin
        infoPrestamo[1] = p.getHoraFecha_Inicio();
        infoPrestamo[2] = p.getHoraFecha_Fin();

        // Tiempo que duró el préstamo --> long
        infoPrestamo[3] = ChronoUnit.MINUTES.between(
                p.getHoraFecha_Inicio(),
                p.getHoraFecha_Fin());

        // Distancia recorrida --> double
        double distancia = Harvesine.calcularDistancia(
                p.getUbicacionInicio(),
                p.getUbicacionFin());
        infoPrestamo[4] = distancia;

        // Valor de pago --> double
        infoPrestamo[5] = p.getValorPago();

        // Batería que utilizó el transporte --> int
        infoPrestamo[6] = p.getTransporte().calcularConsumo(distancia);

        return infoPrestamo;
    }

    public void cambiarTarjetaCredito(Rider r, String nombreBando,
            String numeroTarjeta, String titularTarjeta) {
        r.setNombreBanco(nombreBando);
        r.setNumeroTarjeta(numeroTarjeta);
        r.setTitularTarjeta(titularTarjeta);
    }

    /**
     * Función que recibe un rider que YA HA REALIZADO PRÉSTAMOS y dos Strings
     * que corresponden a un rango de fechas. Compara la fecha de cada uno de
     * los préstamos asociados a ese rider y los agrega a un ArrayList en caso
     * de estar dentro del rango de fechas establecido. Al final retorna el
     * arreglo o null si no existen préstamos en ese rango
     *
     * @param r Rider
     * @param fi fecha / hora de inicio
     * @param ff fecha / hora de fin
     * @return Prestamos p
     */
    public ArrayList<Prestamo> buscarPrestamos(Rider r, String fi,
            String ff) {
        ArrayList<Prestamo> pEncontrados = new ArrayList();

        // Conversión de String a LocalDateTime
        LocalDateTime inicio = sistema.convertirAFecha(fi);
        LocalDateTime fin = sistema.convertirAFecha(ff);

        for (Prestamo p : r.getPrestamos()) {
            if (p.getHoraFecha_Inicio().isAfter(inicio)
                    && p.getHoraFecha_Fin().isBefore(fin)) {
                pEncontrados.add(p);
            }
        }

        ordenarPrestamos(pEncontrados);
        return pEncontrados;

    }

    /**
     * Método que orderna (invierte) un ArrayList de Préstamos, posicionándolos
     * del más reciente al más antiguo.
     *
     * @param p Array de préstamos
     */
    public void ordenarPrestamos(ArrayList<Prestamo> p) {
        // Guarda el último préstamo en una variable 
        Prestamo aux = p.get(p.size() - 1);
        // Inserta al principio el último préstamo
        p.add(0, aux);
        // Elimina el último préstamo
        p.remove(p.size() - 1);
    }

}
