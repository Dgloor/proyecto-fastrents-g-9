package Sistema;

import Modelo.Ubicacion;
import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageForwardRequest;
import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.byteowls.jopencage.model.JOpenCageResponse;
import com.byteowls.jopencage.model.JOpenCageReverseRequest;

/**
 *
 * @author Danny Loor
 */
public class SistemaGPS {

    /**
     * Método que recibe un String direccion y retorna coordenadas.
     *
     * @param direccion String
     * @return Ubicacion ( lat, lon ) 
     */
    public static Ubicacion consultarUbicacion(String direccion) {
        JOpenCageGeocoder jOpenCageGeocoder
                = new JOpenCageGeocoder("7b4c6c7d86b0465fa867f9dc47ec3c2b");

        JOpenCageForwardRequest request;
        Ubicacion ubicacion;

        request = new JOpenCageForwardRequest(direccion + ", Guayaquil");

        // Prioriza los resultados en un lenguaje específico
        request.setLanguage("es");
        // Restringe los resultados a un país específico.
        request.setRestrictToCountryCode("ec");
        // Restringe los resultados con una calificación de confianza de n/10.
        request.setMinConfidence(3);

        JOpenCageResponse respuesta = jOpenCageGeocoder.forward(request);
        // get del par de coordenadas del primer resultado
        JOpenCageLatLng coordenadas = respuesta.getFirstPosition();

        // Transformación de tipo JOpenCageLatLng a Ubicacion.
        ubicacion = new Ubicacion(coordenadas.getLat(), coordenadas.getLng());

        return ubicacion;
    }

    /**
     * Método que recibe coordenadas y devuelve un String direccion.
     *
     * @param u Ubicación
     * @return La dirección como String
     */
    public static String consultarUbicacion(Ubicacion u) {
        JOpenCageGeocoder jOpenCageGeocoder
                = new JOpenCageGeocoder("7b4c6c7d86b0465fa867f9dc47ec3c2b");
        JOpenCageReverseRequest request
                = new JOpenCageReverseRequest(u.getLatitud(), u.getLonguitud());

        request.setLanguage("es");
        // Sólo retornará el primer resultado(10 por default)
        request.setLimit(1);
        // Excluye información extra como el timezone, etc.
        request.setNoAnnotations(true);
        request.setMinConfidence(3);

        JOpenCageResponse respuesta = jOpenCageGeocoder.reverse(request);
        // get de la primera dirección resultante formateada.
        String direccion = respuesta.getResults().get(0).getFormatted();

        return direccion;
    }
}
