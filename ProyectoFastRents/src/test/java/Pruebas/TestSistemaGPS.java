package Pruebas;

import Modelo.Harvesine;
import Sistema.SistemaGPS;
import Modelo.Ubicacion;

/**
 *
 * @author Danny Loor
 */
public class TestSistemaGPS {

    public static void main(String[] args) {
        String direccion = "City Mall";
        Ubicacion coordenadas = new Ubicacion(-2.1408596, -79.9097287);

        Ubicacion ubi = SistemaGPS.consultarUbicacion(direccion);
        String sitio = SistemaGPS.consultarUbicacion(coordenadas);

        System.out.println("Las coordenadas de " + direccion
                + " son:\n" + ubi.toString());

        // sitio tiene muchos datos pero como sé que estoy en el contexto
        // de "Guayaquil, Ecuador" omito esos datos en la impresión
        System.out.println("El sitio al que corresponde la ubicación es: "
                + sitio.split(",")[0] + ".");
        
        
        Ubicacion u1 = SistemaGPS.consultarUbicacion("City Mall");
        Ubicacion u2 = SistemaGPS.consultarUbicacion("Lago ESPOL");
        System.out.println(u1);
        System.out.println(u2);
        
        System.out.println("Diferencia entre City Mall y Lago ESPOL: ");
        double distancia = Harvesine.calcularDistancia(u1, u2);
        System.out.println(distancia);
    }

}
