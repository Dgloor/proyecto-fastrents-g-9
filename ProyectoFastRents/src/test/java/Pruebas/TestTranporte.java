package Pruebas;

import Modelo.Bicicleta;

/**
 *
 * @author Danny Loor
 */
public class TestTranporte {

    public static void main(String[] args) {
        Bicicleta b = new Bicicleta("123", "Lago ESPOL");
        System.out.println(b);
        
        Object[] lista;
        lista = b.reportarInfo();

        System.out.println(lista[0]);
        System.out.println(lista[0].getClass());
        System.out.println(b.calcularConsumo(2)); // bateria consumida
    }

}
