package Pruebas;

import Modelo.Bicicleta;
import Modelo.Prestamo;
import Modelo.Rider;
import java.text.ParseException;
import java.time.LocalDateTime;

/**
 *
 * @author Danny Loor
 */
public class TestPrestamo {

    public static void main(String[] args) throws ParseException {
        
        Bicicleta b =new Bicicleta("5432", "Lago Espol");
        Rider r = new Rider("0953496437", "Danny Loor", "La puntilla",
                "dgloor@espol.edu.ec", "danLoor", "dgloor001",
                "Banco Pacifico", "150235121215", "Mama");
        
        LocalDateTime dt = LocalDateTime.now();  
        Prestamo p = new Prestamo(r, b, dt);   
        System.out.println(p.getHoraFecha_Inicio());
        
        

    }

}
