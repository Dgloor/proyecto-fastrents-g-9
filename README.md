# Proyecto FastRents - G#9 by Danny Loor only

- Al comienzo esperar que se descarguen un par de
dependencias.

Nota: En caso de observar superposiciones de Strings
al momento de correr el programa, volver a ejecutar.
... Errores ajenos al proyecto (neatbeans es medio raro).


-- CREDENCIALES DE USUARIOS --
      (User - password)

Riders:
    rider1 - rider1
    rider2 - rider2

Chargers:
    charger1 / charger1	
    charger2 / charger2

-- CÓDIGOS DE TRANSPORTES --
Cod. de Bicicletas siempre comienzan con b; b001, b002...
Cod. de Scooters siempre comienzan con s; s001, s002...

-- INSTRUCCIONES ADICIONALES --
En el menú incial si se escoge la opción "Inciciar Sesión"
se muestran dos campos a llenar, en caso de querer salir 
pero sin llenar dichos campos, escribir -cualquier cosa-, 
se válida que no existe ese usuario y se regresa al menú,
de igual manera al momento de escoger "Registrarse", y 
más adelante también.

Obviamente se recomienda primero <<realizar un par de
préstamos(y finalizarlos) dentro de la interfaz del Rider
(leer: credenciales de Rider) para proceder a cargar 
transportes luego con un Charger puesto que este al incio
no tiene nada que cargar( todos los transportes tienen 
un 100% de batería), lo mismo si se quieren buscar 
transportes cercanos para cargar
